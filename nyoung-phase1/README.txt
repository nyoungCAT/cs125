Nathan Young
Assistance getting bitbucket set up from Blake Dayman, E/R diagram created with collaboration from my team (Rebecca Frink, Kevin Gao, and James Bek)
https://bitbucket.org/nyoungCAT/cs125/src
The aim of our project is to create a database and associated web pages that will allow efficient acquisition and distribution of books to Third Culture Kid families (TCKs) around the world.  BookEnds International is a non-profit which receives books from donors, and sends them to TCKs.  TCKs are allowed to see the list of available books and choose them.  Books are stored by category.  Shipping charges are shared between TCKs and BookEnds.  Volunteers assist in shelving books, pulling requests, and shipping.  Donors may also donate financially.  Donors need to be given receipts for each donation, and yearly reports of their donations. Major sponsors need yearly reports of BookEnds activities and statistics. Some TCKs need receipts for the cost of their portion of the shipping. 

BookEndsOverview.txt provides a more in-depth explanation of the specifications of BookEnds, their organizational needs, and how this database will solve those needs.

ERDiagram.erdplus is a Entity-Relationship Diagram that maps out the general structure of the database. It can be opened at http://www.erdplus.com/

ERDiagram.png is a png of the same Entity-Relationship Diagram. It can be easily accessed and viewed, but cannot be edited using the ERDPlus tool online.



 
